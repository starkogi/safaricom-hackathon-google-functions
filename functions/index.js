const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//


exports.safCallBack = functions.https.onRequest((request, response) => {

	// Check for POST request
	if(request.method !== "POST"){
	 res.status(400).send('Please send a POST request');
	 return;
	}else{

		let data = request.body;

		admin.database().ref("/safCallBack/").push().set(data.Body.stkCallback);
	 	response.send("Hello from Firebase!");
	}
 	

});




